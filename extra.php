<?php

/**
 * This will return some information necessary for the hook_forms hook. This information is 
 * important to be able to handle multiple entity connections and relation deletions in one form
 */
function econnect_forms($form_id, $args){
  $forms = array();
  if (strpos($form_id, 'econnect__action_form_') === 0) {
    $forms[$form_id] = array(
      'callback' => 'econnect__action_form'
    );
  }
  
  if (strpos($form_id, 'econnect__action_delete_form_') === 0) {
    $forms[$form_id] = array(
      'callback' => 'econnect__action_delete_form'
    );
  }
  return $forms;
}

/**
 * This function creates a page that allows you to connect a target type to some entities. for example
 * I could connect gallery images to a vendor, in this case $entity_type, $bundle, and $ids would be used
 * to get the VENDORS we are intereseted on, and $target_type and $target_bundle would tell us which type
 * of entity we want to connect to the VENDORS, in our example it would be a GALLERY entity of a certain type.
 */
function econnect__action($id, $info){
  //dpm($info, "INFO");
   $entity_type = $info['entity_type'];
   
  if(is_numeric($id)){
    $build = array();
   
    
    $target_type = $info['target_entity_type'];
    $target_bundle = $info['target_bundle'];
    //Ok first lets get the main entity to which we are connecting stuff
    $info['id'] = $id;
    $entity = entity_load($entity_type, array($id));
    $entity = $entity[$id];
    //I don't understand why this is not done automatically, if you get an entity
    //it is impossible to find out what it is, so here it goes we let the entity know
    //of what kind it is.
    $entity->entity_type = $entity_type;
    
    $build[$id]['div1'] = array('#markup' => "<div style='position: relative; float:left; 
      background-color:white; margin:10px; padding: 10px; border-style:solid; border-width:1px; height: 520px'>");
      
    //@TODO Should this be optional, maybe in some cases we don't want to see anything
    //about the entity that we are connecting things too.
    $label = entity_label($entity_type, $entity);
    $build[$id]['entity'] = array('#markup' => "<h1>{$label}</h1>");
    
    module_load_include('inc', 'econnect', 'econnect.util');
      
    $related = relation_get_related($entity, $target_type, NULL, $target_bundle);
    
    if($info['mode'] == 'single'){
      $build[$id]['form'] = drupal_get_form("econnect__action_form_{$id}",$entity, $info);
    }
    $weight = 20000;
      
    //Get each of the related entities $rentity, and set up a form to delete that
    //relation
    foreach($related as $rid => $rentity){
     $build[$id]['related_'.$rid] = drupal_get_form('econnect__action_delete_form_'.$id.'_'.$rid, $entity, $rentity, $info);
    }
    
    $build[$id]['div2'] = array('#markup' => "</div>");
    
    return $build;
  }else{
    $ids = explode(" ", $id);
    $build = array();
    
    if($info['mode'] == 'multiple'){
      //we need to check that all ids are numeric
      $final_ids = array();
      foreach($ids as $id){
        if(is_numeric($id)){
          $final_ids[] = $id;
        }
      }
      
      //lets load the entities
      $entities = entity_load($entity_type, $final_ids);
      $info['id'] = $final_ids;
      $build[$id]['form'] = drupal_get_form("econnect__action_form_{$id}",$entities, $info);
    
    }
    
    
    foreach($ids as $id){
      if(is_numeric($id)){
        $build = array_merge($build, econnect__action($id, $info));
      }
    }
    
    return $build;
  }
}

/*
 * This form is the one used when a connect action is performed
 * 
 * @arg $entity The entity object as returned by entity_load, this is the
 * entity we want to connect things too. NOTE: the entity should have a variable
 * 'entity_type' that holds the type of this entity
 * 
 * @arg $info is an array with four keys:
 * 'target_entity_type': the type of entities that we will be connecting to entity
 * 'target_entity_bundle': the bundle of the target entity type that we will be
 * connecting to entity
 * 'relation_type': the relation type that this connection will create
 * 'autocomplete': this is a callback function to the autocomplete mechanism to choose
 * which entity of target_entity_type, and target_bundle we are connectin to the entity
 * 
 * This array can also have the option key
 * 
 * 'autocomplete_callback': This is a function that will be called to get the id value
 * from what autocomplete returns, if this is not set, we will assume that autocomplete
 * is returning the id and no processing is needed
 */
function econnect__action_form($form, $state, $entity, $info){
  //dpm($entity, "Entity");
  //dpm($info, "Info");
  
  $form['entity'] = array('#type' => 'value', '#value' => $entity);
  $form['info'] = array('#type' => 'value', '#value' => $info);
  $target_type = $info['target_entity_type'];
  
  $form[$target_type] = array(
    '#title' => t("Select a {$target_type}"),
    '#type' => 'textfield',
    '#autocomplete_path' => $info['autocomplete'],
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#weight' => 10000,
    '#value' => t('Connect'),
  );
  
  
  return $form;
}

function econnect__action_form_submit($form, $state){
  
  $entity = $state['values']['entity'];
  $info = $state['values']['info'];
  
  $type = $info['entity_type'];
  $bundle = $info['bundle'];
  $ids = $info['id'];
  if(!is_array($ids)){
    $ids = array($ids);
  }
  
  $ttype = $info['target_entity_type'];
  $tbundle = $info['target_bundle'];
  
  $tid = $state['values'][$ttype];
  
  if(array_key_exists('autocomplete_callback', $info)){
    $tid = call_user_func($info['autocomplete_callback'], $tid);
  }
  
  foreach($ids as $id){
    $endpoints = array();
    
    $endpoints[0] = array(
      'entity_type' => $type,
      'entity_id' => $id,
      'entity_bundle' => $bundle,
      'r_index' => 0,
    );
    
    $endpoints[1] = array(
      'entity_type' => $ttype,
      'entity_id' => $tid,
      'entity_bundle' => $tbundle,
      'r_index' => 1,
    );
    
    relation_save(relation_create($info['relation_type'], $endpoints));
  }
}

/**
 * From the connect action form you can also delete a relation
 * this form handles the deletion
 */
function econnect__action_delete_form($form, &$state, $entity, $etarget, $info){
  $form['entity'] = array(
    '#type' => 'value',
    '#value' => $entity,  
  );
  
  $form['entity_target'] = array(
    '#type' => 'value',
    '#value' => $etarget,  
  );
  
  $form['info'] = array(
    '#type' => 'value',
    '#value' => $info,  
  );
  
  $form['label'] = array(
    '#markup' => entity_label($info['target_entity_type'], $etarget)." "
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );
  
  return $form;
}

function econnect__action_delete_form_submit($form, &$state){
  module_load_include('inc', 'econnect', 'econnect.util');
  $entity = $state['values']['entity'];
  $target = $state['values']['entity_target'];
  $info = $state['values']['info'];
  
  //dpm($info, "Info");
 
  $query = new EntityFieldQuery();
  $query
  ->entityCondition('entity_type', 'relation', '=')
  ->fieldCondition('endpoints', 'entity_type', $info['entity_type'], '=', 0)  
  ->fieldCondition('endpoints', 'entity_id', $info['id'], '=', 0)  
  ->fieldCondition('endpoints', 'entity_type', $info['target_entity_type'], '=',1)  
  ->fieldCondition('endpoints', 'entity_id', entity_get_id($info['target_entity_type'], $target) , '=', 1);
  
  $results = $query->execute();
  //dpm($results, "Results");
  if(count($results['relation']) > 1){
    drupal_set_message("There shouldn't be more than one relation to be deleted", "error");
  }else{
    foreach($results['relation'] as $rid => $info){
      relation_delete($rid);
    }
  }
}

function econnect_econnect_info(){
  $info['user/%/articles'] =
  array(
  'mode' => 'single',
  'relation_type' => 'connect',
  'label' => 'articles',
  'autocomplete' => "entity-autocomplete/bundle/node/article",
  'entity_type' => 'user',
  'bundle' => 'user',
  //This is a url arg, so it will be the wildcard in this case
  'id' => 1,
  'target_entity_type' => 'node',
  'target_bundle' => 'article',
  //optional
  'autocomplete_callback' => 'entity_autocomplete_get_id'
  );
  
  return $info;
}

