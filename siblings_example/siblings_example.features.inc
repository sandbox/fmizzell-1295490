<?php
/**
 * @file
 * siblings_example.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function siblings_example_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "relation" && $api == "relation_type_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function siblings_example_node_info() {
  $items = array(
    'siblings' => array(
      'name' => t('Siblings'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
